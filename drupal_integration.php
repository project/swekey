<?php

include(dirname(__FILE__).'/swekey_integration.php');

// OPTIONAL
// include your include files here

class DrupalSwekeyIntegration extends SwekeyIntegration
{	
	function DrupalSwekeyIntegration()
	{
	    //$this->show_debug_info = true;

		// REQUIRED
		// Put the relative URL of your swekey directory.
		// This is used load the swekey javascript files
	  	$this->swekey_dir_url = base_path().drupal_get_path('module', 'swekey').'/';

		// REQUIRED
		// Put the name of you user name text input of your login form here.
		// This is used to auto-fill the username when a swekey is plugged
		// You can use multiple names
	 	$this->input_names = array("edit-name", "edit[name]", "name");		

		// REQUIRED
		// Set this value to true if a user is logged
		global $user;
		$this->is_user_logged = ($user->uid > 0);
		
		if ($this->is_user_logged)
		{
			// REQUIRED
			// Provide an URL that should be used to logout the current user
			// This is used when a user unplug its swekey
			$this->logout_url = '?q=logout';

			// REQUIRED
			// If the logged user has a swekey associated with his account 
			// fill this value with the swekey if
            $this->swekey_id_of_logged_user = @$_SESSION['user_swekey_id'];

		}
		
		// OPTIONAL
	    // Set this value to the current locale
	    // more than one username/password form
	 	//$this->lang = 'en-US';

		// OPTIONAL
	    // Set this member to true if your login window contains 
	    // more than one username/password form
	 	//$this->multiple_logos = true;

		// Debug
	    // To enable logging set the following var to the path of your log file
	 	//$this->logFile = 'c:\\logs\\qwe.log';
	 	//$this->LogStr('Start');
	}

	// REQUIRED 
	// Return the name of the user from a given swekey id 
	// This is used to auto-fill the username when a swekey is plugged
	function GetUserNameFromSwekeyId($swekey_id)
	{
		$result = db_query('SELECT uid FROM {swekey_ids} WHERE swekey_id = "%s"', $swekey_id);
		if ($field = db_fetch_object($result)) 
		{
			$result = db_query('SELECT name FROM {users} WHERE uid = %d', $field->uid);
			if ($field = db_fetch_object($result)) 
			{
				return $field->name;
	        }
	  	}
	}
	
	// REQUIRED
	// Set the swekey_id of the current user
	function AttachSwekeyToCurrentUser($swekey_id)
	{
		global $user;
		db_query('DELETE FROM {swekey_ids} WHERE uid = %d', $user->uid);
		db_query("INSERT INTO {swekey_ids} (uid, swekey_id) VALUES ('%s', '%s')", $user->uid, $swekey_id);
		$user->swekey_id = $swekey_id;
	}
	

	function GetConfig()
	{	
		$config = array
		(
		    'check_server' => variable_get('swekey_check_Server', 'http://auth-check.musbe.net'),
		    'rndtoken_server' => variable_get('swekey_rndtoken_Server', 'http://auth-rnd-gen.musbe.net'),
		    'status_server' => variable_get('swekey_status_server', 'http://auth-status.musbe.net'),
		    'allow_disabled' => variable_get('swekey_allow_disabled', 1),
		    'user_managment' => variable_get('swekey_user_managment', 1),
		    'brands' => variable_get('swekey_brands', ''),
		    'logo_url' => variable_get('swekey_logo_url', 'http://www.swekey.com?promo=drupal'),
		    'logo_xoffset' => variable_get('swekey_logo_xoffset', '0px'),
		    'logo_yoffset' => variable_get('swekey_logo_yoffset', '-1px'),
		    'loginname_width_offset' => variable_get('swekey_loginname_width_offset', ''),
		    'show_only_plugged' => variable_get('swekey_show_only_plugged', 0),
		    'no_linked_otp' => variable_get('swekey_no_linked_otp', 0),
		    'https_server_hostname' => variable_get('swekey_https_server_hostname', ''),
		    'allow_mobile_emulation' => variable_get('swekey_allow_mobile_emulation', 1),
		    'allow_when_no_network' => variable_get('swekey_allow_when_no_network', 0),
		 );
		return $config;
	}
	
	





	
}	

?>
