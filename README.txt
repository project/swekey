/* $Id$ */

Name:Swekey Module 
Version:2.9 
Revision:4909
Updated:05/31/11

This is a login Module that add support for the swekey authentication key.
Once installed, users that own a swekey can harden their account access
using the swekey.
Activating this module does not make any difference for non swekey users.
All swekey information can be found at http://www.swekey.com

Installation
------------

Copy the swekey directory into your module directory and then enable it using
the admin modules page.

Version History
---------------

2.9
	Changed version numbering
	Smartphones support implemented
2.0.4  (6.x-2.0-rc4)
	Code shared between drupal 6 and 7
2.0.1  (6.x-2.0-rc1)
	Used the new integration kit
1.0.11  (6.x-1.0-rc11)
	Sometimes the login name was still not resolved
1.0.9  (6.x-1.0-rc9)
	Changed load order (it is now minimal).
	Sometimes the login name was not resolved
1.0.8  (6.x-1.0-rc3)
	Fixed a potential security bug,.
1.0.7
	Added support for Logo Link (parnership program)
1.0.6
	Fixed a path issue (thanks to lacmac)
1.0.5
	Swekey logo is no longer retreived from swekey servers
1.0.4
	Added error message when user try to relogin rigth after a swekey unplug 
1.0.3
	Fixed a bug that prevented the plugin to work with /nodes path set
1.0.2 
	Added support for brands
1.0.1 
	Added configuration page
	Added musbe trusted root certificate for https server access
1.0.0 
	First release

Author
------
Musbe, Inc.
support@swekey.com
